# README #

This is simple WPF application written to test what can be done using Caliburn.Micro library. Technically it is not finished and UI will burn your eyes, but is is functional for presentation purpose using dedicated FakeRepository . The purpose of this is to help someone train english language / vocabulary together with any book. Application allows user to insert new words and expressions, their respective translations and types and then play Duolingo-like memory game.

### How do I get set up? ###

* Prerequisites: .NET Framework 4.7.1 runtime, Visual Studio
* Download ZIP or clone repository,
* Open VocabularyTrainerV2.sln
* Run and enjoy!

### Additional packages used in the project ###

* [Caliburn.micro](https://caliburnmicro.com/)
* [Entity Framework](https://www.entityframeworktutorial.net/what-is-entityframework.aspx)